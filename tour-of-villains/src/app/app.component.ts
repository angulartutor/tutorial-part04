import { Component, OnInit } from '@angular/core';
import { Villain } from './villain';
import { VillainService } from './villain.service';


@Component({
  selector: 'app-root',
  template: `
  <h1>{{title}}</h1>
  
  <h2>My Villains</h2>
  <ul class="villains">
    <li *ngFor="let v of villains" 
               [class.selected]="v === selectedVillain"
               (click)="onSelect(v)">
      <span class="badge">{{v.id}}</span> {{v.name}}
    </li>
  </ul>  


  <app-villain-detail [villain]="selectedVillain"></app-villain-detail>

  <app-messages></app-messages>

  `,
  styles: [`
  .selected {
    background-color: #CFD8DC !important;
    color: white;
  }
  .villains {
    margin: 0 0 2em 0;
    list-style-type: none;
    padding: 0;
    width: 15em;
  }
  .villains li {
    cursor: pointer;
    position: relative;
    left: 0;
    background-color: #EEE;
    margin: .5em;
    padding: .3em 0;
    height: 1.6em;
    border-radius: 4px;
  }
  .villains li.selected:hover {
    background-color: #BBD8DC !important;
    color: white;
  }
  .villains li:hover {
    color: #607D8B;
    background-color: #DDD;
    left: .1em;
  }
  .villains .text {
    position: relative;
    top: -3px;
  }
  .villains .badge {
    display: inline-block;
    font-size: small;
    color: white;
    padding: 0.8em 0.7em 0 0.7em;
    background-color: #607D8B;
    line-height: 1em;
    position: relative;
    left: -1px;
    top: -4px;
    height: 1.8em;
    margin-right: .8em;
    border-radius: 4px 0 0 4px;
  }
`]
})
export class AppComponent implements OnInit {
  title = 'Tour of Villains';
  selectedVillain: Villain;
  // villains = VILLAINS;
  villains: Villain[];

  constructor(private villainService: VillainService) {

  }

  private _initVillains(): void {
    this.villainService.getVillains()
    .subscribe(villains => this.villains = villains);
  }

  ngOnInit(): void {
    this._initVillains();
  }

  onSelect(villain: Villain): void {
    this.selectedVillain = villain;
  }
}
