import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { Villain } from '../villain';

@Component({
  selector: 'app-villain-detail',
  templateUrl: './villain-detail.component.html',
  styleUrls: ['./villain-detail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class VillainDetailComponent implements OnInit {
  @Input()
  villain: Villain;
  
  constructor() { }

  ngOnInit() {
  }

}
